default: build dockerize

build:
	mvn clean package -U -Dmaven.test.skip=true

dockerize:
	docker build -f logmapsystemadapter.docker -t git.project-hobbit.eu:4567/ernestoj/logmapsystem .

	docker push git.project-hobbit.eu:4567/ernestoj/logmapsystem
