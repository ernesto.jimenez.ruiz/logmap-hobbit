# LogMap System for the HOBBIT-OAEI evaluation campaign

This **maven project** contains the necessary classes to implement the **logmap-hobbit** system interface to be run under the [HOBBIT platform](https://github.com/hobbit-project/platform/wiki/Upload-a-benchmark). 

LogMap source codes can be found [here](https://github.com/ernestojimenezruiz/logmap-matcher).


### System class

The interface of LogMap is defined via the **LogMapSystemAdapter** class. This class receives the dataset definition from the **DataGenerator** of a benchmark (e.g. set of tasks) and the individuals tasks (source and target datasets) from the **TaskGenerator** of a benchmark. The results (a file containing the mappings in RDF Alignment format) are sent to the benchmark's **EvaluationModule**. See details of the [HOBBIT largebio benchmark here](https://gitlab.com/ernesto.jimenez.ruiz/largebio).

The **system adapter** class communicates to the benchmark classes in a special way since they are submitted to the HOBBIT platform	as docker images: [https://github.com/hobbit-project/platform/wiki/Push-a-docker-image](https://github.com/hobbit-project/platform/wiki/Push-a-docker-image)


### System metadata

The system metadata is described in the **system.ttl** file (see more details [here](https://github.com/hobbit-project/platform/wiki/System-meta-data-file)). 
This file should be added to a HOBBIT gitlab project (e.g. [https://git.project-hobbit.eu/ernestoj/logmapsystem](https://git.project-hobbit.eu/ernestoj/logmapsystem)).
To do so you should first create a user: [https://github.com/hobbit-project/platform/wiki/User-registration](https://github.com/hobbit-project/platform/wiki/User-registration)

The system metadata should explicitly mention that implements the API of one or more benchmarks (e.g. **hobbit:implementsAPI	bench:LargebioAPI;**. In 
practical terms the System adapter should also interpret the information that is sent 
from the DataGenerator and TaskGenerator of the implemented benchmarks.


### Docker file and submission to the HOBBIT platform

Once the **SystemAdapter** class has been implemented and a project has been created in the HOBBIT gitlab (e.g. [https://git.project-hobbit.eu/ernestoj/logmapsystem](https://git.project-hobbit.eu/ernestoj/logmapsystem). The next step is to create the docker image for each of the system adapter class (e.g. **logmapsystemadapter.docker**) and push them to the HOBBIT platform. The docker file assume that a **maven package** for the system has been created (e.g. target/logmap-hobbit-1.0.0-SNAPSHOT.jar).

The **Makefile** file contains the commands to create and push the docker images. For more information see [https://github.com/hobbit-project/platform/wiki/Push-a-docker-image](https://github.com/hobbit-project/platform/wiki/Push-a-docker-image)

To be allowed to push the docker images, you must use a personal access token with 'api' scope for **Git** over HTTP:
* Generate one at [https://git.project-hobbit.eu/profile/personal_access_tokens](https://git.project-hobbit.eu/profile/personal_access_tokens)
* Run ''docker login git.project-hobbit.eu:4567''. Use your HOBBIT gitlab user (e.g. ernestoj) and the generated token.
* Execute the Makefile (e.g. make).

In addition, Docker may require to be executed with root privileges.

If not you have not done it before you have to [install Docker CE](https://docs.docker.com/engine/installation/).


### HOBBIT gitlab projects

Relevant projects to the OAEI can be found under the [OAEI group](https://git.project-hobbit.eu/OAEI).


### HOBBIT online instance

The online instance of the HOBBIT benchmarking platform is accessible at [master.project-hobbit.eu](https://master.project-hobbit.eu/home).
